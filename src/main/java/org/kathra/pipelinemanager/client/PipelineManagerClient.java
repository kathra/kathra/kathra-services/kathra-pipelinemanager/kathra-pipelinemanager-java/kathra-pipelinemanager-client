/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.pipelinemanager.client;

import org.kathra.client.*;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import org.kathra.utils.KathraSessionManager;
import org.kathra.utils.ApiException;
import org.kathra.utils.ApiResponse;

import org.kathra.core.model.Build;
import org.kathra.pipelinemanager.model.Credential;
import org.kathra.core.model.Membership;
import org.kathra.core.model.Pipeline;

public class PipelineManagerClient {
    private ApiClient apiClient;

    public PipelineManagerClient() {
        this.apiClient = new ApiClient().setUserAgent("PipelineManagerClient 1.2.0");
    }

    public PipelineManagerClient(String serviceUrl) {
        this();
        apiClient.setBasePath(serviceUrl);
    }

    public PipelineManagerClient(String serviceUrl, KathraSessionManager sessionManager) {
        this(serviceUrl);
        apiClient.setSessionManager(sessionManager);
    }

    public ApiClient getApiClient() {
        return apiClient;
    }

    private void setApiClient(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    /**
     * Build call for addCredential
     * @param credential Credential to create (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call addCredentialCall(Credential credential, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = credential;
        
        // create path and map variables
        String localVarPath = "/credentials";

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call addCredentialValidateBeforeCall(Credential credential, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'credential' is set
        if (credential == null) {
            throw new ApiException("Missing the required parameter 'credential' when calling addCredential(Async)");
        }
        
        
        com.squareup.okhttp.Call call = addCredentialCall(credential, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Creates a credential for a specified folder
     * 
     * @param credential Credential to create (required)
     * @return Credential
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public Credential addCredential(Credential credential) throws ApiException {
        ApiResponse<Credential> resp = addCredentialWithHttpInfo(credential);
        return resp.getData();
    }

    /**
     * Creates a credential for a specified folder
     * 
     * @param credential Credential to create (required)
     * @return ApiResponse&lt;Credential&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<Credential> addCredentialWithHttpInfo(Credential credential) throws ApiException {
        com.squareup.okhttp.Call call = addCredentialValidateBeforeCall(credential, null, null);
        Type localVarReturnType = new TypeToken<Credential>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Creates a credential for a specified folder (asynchronously)
     * 
     * @param credential Credential to create (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call addCredentialAsync(Credential credential, final ApiCallback<Credential> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = addCredentialValidateBeforeCall(credential, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<Credential>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for addMembership
     * @param membership Credential to create (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call addMembershipCall(Membership membership, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = membership;
        
        // create path and map variables
        String localVarPath = "/memberships";

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call addMembershipValidateBeforeCall(Membership membership, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'membership' is set
        if (membership == null) {
            throw new ApiException("Missing the required parameter 'membership' when calling addMembership(Async)");
        }
        
        
        com.squareup.okhttp.Call call = addMembershipCall(membership, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Creates a membership for specified folder
     * 
     * @param membership Credential to create (required)
     * @return Membership
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public Membership addMembership(Membership membership) throws ApiException {
        ApiResponse<Membership> resp = addMembershipWithHttpInfo(membership);
        return resp.getData();
    }

    /**
     * Creates a membership for specified folder
     * 
     * @param membership Credential to create (required)
     * @return ApiResponse&lt;Membership&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<Membership> addMembershipWithHttpInfo(Membership membership) throws ApiException {
        com.squareup.okhttp.Call call = addMembershipValidateBeforeCall(membership, null, null);
        Type localVarReturnType = new TypeToken<Membership>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Creates a membership for specified folder (asynchronously)
     * 
     * @param membership Credential to create (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call addMembershipAsync(Membership membership, final ApiCallback<Membership> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = addMembershipValidateBeforeCall(membership, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<Membership>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for createBuild
     * @param build Build to run, must contain path & branch (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call createBuildCall(Build build, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = build;
        
        // create path and map variables
        String localVarPath = "/builds";

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call createBuildValidateBeforeCall(Build build, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'build' is set
        if (build == null) {
            throw new ApiException("Missing the required parameter 'build' when calling createBuild(Async)");
        }
        
        
        com.squareup.okhttp.Call call = createBuildCall(build, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Creates a Build
     * 
     * @param build Build to run, must contain path & branch (required)
     * @return Build
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public Build createBuild(Build build) throws ApiException {
        ApiResponse<Build> resp = createBuildWithHttpInfo(build);
        return resp.getData();
    }

    /**
     * Creates a Build
     * 
     * @param build Build to run, must contain path & branch (required)
     * @return ApiResponse&lt;Build&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<Build> createBuildWithHttpInfo(Build build) throws ApiException {
        com.squareup.okhttp.Call call = createBuildValidateBeforeCall(build, null, null);
        Type localVarReturnType = new TypeToken<Build>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Creates a Build (asynchronously)
     * 
     * @param build Build to run, must contain path & branch (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call createBuildAsync(Build build, final ApiCallback<Build> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = createBuildValidateBeforeCall(build, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<Build>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for createFolder
     * @param path Folder path (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call createFolderCall(String path, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = path;
        
        // create path and map variables
        String localVarPath = "/folders";

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call createFolderValidateBeforeCall(String path, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'path' is set
        if (path == null) {
            throw new ApiException("Missing the required parameter 'path' when calling createFolder(Async)");
        }
        
        
        com.squareup.okhttp.Call call = createFolderCall(path, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Create a new folder
     * 
     * @param path Folder path (required)
     * @return Boolean
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public Boolean createFolder(String path) throws ApiException {
        ApiResponse<Boolean> resp = createFolderWithHttpInfo(path);
        return resp.getData();
    }

    /**
     * Create a new folder
     * 
     * @param path Folder path (required)
     * @return ApiResponse&lt;Boolean&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<Boolean> createFolderWithHttpInfo(String path) throws ApiException {
        com.squareup.okhttp.Call call = createFolderValidateBeforeCall(path, null, null);
        Type localVarReturnType = new TypeToken<Boolean>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Create a new folder (asynchronously)
     * 
     * @param path Folder path (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call createFolderAsync(String path, final ApiCallback<Boolean> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = createFolderValidateBeforeCall(path, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<Boolean>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for createPipeline
     * @param pipeline Pipeline to create, must contain path & source repository (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call createPipelineCall(Pipeline pipeline, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = pipeline;
        
        // create path and map variables
        String localVarPath = "/pipelines";

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call createPipelineValidateBeforeCall(Pipeline pipeline, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'pipeline' is set
        if (pipeline == null) {
            throw new ApiException("Missing the required parameter 'pipeline' when calling createPipeline(Async)");
        }
        
        
        com.squareup.okhttp.Call call = createPipelineCall(pipeline, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Create CI/CD pipeline for an existing source
     * 
     * @param pipeline Pipeline to create, must contain path & source repository (required)
     * @return Pipeline
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public Pipeline createPipeline(Pipeline pipeline) throws ApiException {
        ApiResponse<Pipeline> resp = createPipelineWithHttpInfo(pipeline);
        return resp.getData();
    }

    /**
     * Create CI/CD pipeline for an existing source
     * 
     * @param pipeline Pipeline to create, must contain path & source repository (required)
     * @return ApiResponse&lt;Pipeline&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<Pipeline> createPipelineWithHttpInfo(Pipeline pipeline) throws ApiException {
        com.squareup.okhttp.Call call = createPipelineValidateBeforeCall(pipeline, null, null);
        Type localVarReturnType = new TypeToken<Pipeline>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Create CI/CD pipeline for an existing source (asynchronously)
     * 
     * @param pipeline Pipeline to create, must contain path & source repository (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call createPipelineAsync(Pipeline pipeline, final ApiCallback<Pipeline> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = createPipelineValidateBeforeCall(pipeline, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<Pipeline>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for deletePipeline
     * @param path Pipeline path (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call deletePipelineCall(String path, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // create path and map variables
        String localVarPath = "/pipelines/{path}"
            .replaceAll("\\{" + "path" + "\\}", apiClient.escapeString(path.toString()));

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "DELETE", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call deletePipelineValidateBeforeCall(String path, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'path' is set
        if (path == null) {
            throw new ApiException("Missing the required parameter 'path' when calling deletePipeline(Async)");
        }
        
        
        com.squareup.okhttp.Call call = deletePipelineCall(path, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Delete pipeline
     * 
     * @param path Pipeline path (required)
     * @return String
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public String deletePipeline(String path) throws ApiException {
        ApiResponse<String> resp = deletePipelineWithHttpInfo(path);
        return resp.getData();
    }

    /**
     * Delete pipeline
     * 
     * @param path Pipeline path (required)
     * @return ApiResponse&lt;String&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<String> deletePipelineWithHttpInfo(String path) throws ApiException {
        com.squareup.okhttp.Call call = deletePipelineValidateBeforeCall(path, null, null);
        Type localVarReturnType = new TypeToken<String>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Delete pipeline (asynchronously)
     * 
     * @param path Pipeline path (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call deletePipelineAsync(String path, final ApiCallback<String> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = deletePipelineValidateBeforeCall(path, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<String>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for getBuild
     * @param path Build identifier (required)
     * @param buildNumber Build identifier (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call getBuildCall(String path, String buildNumber, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // create path and map variables
        String localVarPath = "/pipelines/{path}/builds/{buildNumber}"
            .replaceAll("\\{" + "path" + "\\}", apiClient.escapeString(path.toString()))
            .replaceAll("\\{" + "buildNumber" + "\\}", apiClient.escapeString(buildNumber.toString()));

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call getBuildValidateBeforeCall(String path, String buildNumber, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'path' is set
        if (path == null) {
            throw new ApiException("Missing the required parameter 'path' when calling getBuild(Async)");
        }
        
        // verify the required parameter 'buildNumber' is set
        if (buildNumber == null) {
            throw new ApiException("Missing the required parameter 'buildNumber' when calling getBuild(Async)");
        }
        
        
        com.squareup.okhttp.Call call = getBuildCall(path, buildNumber, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Gets a Build
     * 
     * @param path Build identifier (required)
     * @param buildNumber Build identifier (required)
     * @return Build
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public Build getBuild(String path, String buildNumber) throws ApiException {
        ApiResponse<Build> resp = getBuildWithHttpInfo(path, buildNumber);
        return resp.getData();
    }

    /**
     * Gets a Build
     * 
     * @param path Build identifier (required)
     * @param buildNumber Build identifier (required)
     * @return ApiResponse&lt;Build&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<Build> getBuildWithHttpInfo(String path, String buildNumber) throws ApiException {
        com.squareup.okhttp.Call call = getBuildValidateBeforeCall(path, buildNumber, null, null);
        Type localVarReturnType = new TypeToken<Build>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Gets a Build (asynchronously)
     * 
     * @param path Build identifier (required)
     * @param buildNumber Build identifier (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call getBuildAsync(String path, String buildNumber, final ApiCallback<Build> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = getBuildValidateBeforeCall(path, buildNumber, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<Build>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for getBuilds
     * @param path Pipeline path (required)
     * @param branch Builded branch (optional)
     * @param maxResult Max results (optional, default to 5)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call getBuildsCall(String path, String branch, Integer maxResult, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // create path and map variables
        String localVarPath = "/pipelines/{path}/builds"
            .replaceAll("\\{" + "path" + "\\}", apiClient.escapeString(path.toString()));

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();
        if (branch != null)
        localVarQueryParams.addAll(apiClient.parameterToPair("branch", branch));
        if (maxResult != null)
        localVarQueryParams.addAll(apiClient.parameterToPair("maxResult", maxResult));

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call getBuildsValidateBeforeCall(String path, String branch, Integer maxResult, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'path' is set
        if (path == null) {
            throw new ApiException("Missing the required parameter 'path' when calling getBuilds(Async)");
        }
        
        
        com.squareup.okhttp.Call call = getBuildsCall(path, branch, maxResult, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Gets a list of Builds
     * 
     * @param path Pipeline path (required)
     * @param branch Builded branch (optional)
     * @param maxResult Max results (optional, default to 5)
     * @return List&lt;Build&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public List<Build> getBuilds(String path, String branch, Integer maxResult) throws ApiException {
        ApiResponse<List<Build>> resp = getBuildsWithHttpInfo(path, branch, maxResult);
        return resp.getData();
    }

    /**
     * Gets a list of Builds
     * 
     * @param path Pipeline path (required)
     * @param branch Builded branch (optional)
     * @param maxResult Max results (optional, default to 5)
     * @return ApiResponse&lt;List&lt;Build&gt;&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<List<Build>> getBuildsWithHttpInfo(String path, String branch, Integer maxResult) throws ApiException {
        com.squareup.okhttp.Call call = getBuildsValidateBeforeCall(path, branch, maxResult, null, null);
        Type localVarReturnType = new TypeToken<List<Build>>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Gets a list of Builds (asynchronously)
     * 
     * @param path Pipeline path (required)
     * @param branch Builded branch (optional)
     * @param maxResult Max results (optional, default to 5)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call getBuildsAsync(String path, String branch, Integer maxResult, final ApiCallback<List<Build>> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = getBuildsValidateBeforeCall(path, branch, maxResult, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<List<Build>>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for getMemberships
     * @param path Pipeline path (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call getMembershipsCall(String path, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // create path and map variables
        String localVarPath = "/folders/{path}/memberships"
            .replaceAll("\\{" + "path" + "\\}", apiClient.escapeString(path.toString()));

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call getMembershipsValidateBeforeCall(String path, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'path' is set
        if (path == null) {
            throw new ApiException("Missing the required parameter 'path' when calling getMemberships(Async)");
        }
        
        
        com.squareup.okhttp.Call call = getMembershipsCall(path, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Get memberships for specified folder
     * 
     * @param path Pipeline path (required)
     * @return List&lt;Membership&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public List<Membership> getMemberships(String path) throws ApiException {
        ApiResponse<List<Membership>> resp = getMembershipsWithHttpInfo(path);
        return resp.getData();
    }

    /**
     * Get memberships for specified folder
     * 
     * @param path Pipeline path (required)
     * @return ApiResponse&lt;List&lt;Membership&gt;&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<List<Membership>> getMembershipsWithHttpInfo(String path) throws ApiException {
        com.squareup.okhttp.Call call = getMembershipsValidateBeforeCall(path, null, null);
        Type localVarReturnType = new TypeToken<List<Membership>>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Get memberships for specified folder (asynchronously)
     * 
     * @param path Pipeline path (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call getMembershipsAsync(String path, final ApiCallback<List<Membership>> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = getMembershipsValidateBeforeCall(path, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<List<Membership>>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
}
